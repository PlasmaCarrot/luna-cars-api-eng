System Overview
=========================

Glossary
-----------------

+------------------+--------------------------------------------------------------------+
|**Term**          | **Description**                                                    |
+==================+====================================================================+
|JSON              |Open standard file format and data interchange format that uses     |
|                  |                                                                    |
|                  |human-readable text to store and transmit data objects.             |
+------------------+--------------------------------------------------------------------+
|Descriptor        |A data set in a closed, binary format, prepared by the vehicle and  |
|                  |                                                                    |
|                  |LP recognition system based on the analyzed characteristics.        |
+------------------+--------------------------------------------------------------------+
|Classifier        |A system parameter that recognizes one of the vehicle or license    |
|                  |                                                                    |
|                  |plate attributes.                                                   |
+------------------+--------------------------------------------------------------------+
|LP                |License plate. A vehicle registration plate.                        | 
+------------------+--------------------------------------------------------------------+

Introduction
-----------------

This document contains general description of the LUNA CARS.API 0.0.12 service.

The document is intended to explain to the user basic functionality of the service for detecting and recognizing vehicle and license plate.

General information
--------------------------

**VisionLabs LUNA CARS** CARS is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS.Analytics, CARS.API and CARS.Stream.

**VisionLabs CARS API** is a vehicle recognition service that allows in real-time following actions:

* identify vehicle brands and models;

* detect license plate;

* determine whether the vehicle belongs to public transport, taxi, or emergency vehicles;

* evaluate the quality of incoming images;

* determine the conditions for issuing a fine for vehicles that have violated the speed limit;

* checking the intersection of a solid line;

* determine type of vehicle;

* determine color of vehicle;

* extract the vehicle descriptor;

* determine country of vehicle registration.

1. General Descriptions of CARS API
-------------------------------------------

CARS API works with images of vehicles obtained from traffic cameras or other recording devices.

There are several requirements for images:

* Images must be encoded in base64 format;

* Vehicle and license plate should be clearly visible.

The general scheme of the CARS.API is shown in Figure 1.

1. Clients send a POST request in JSON format to CARS.API via the Nginx balancer, containing information about the required classifiers and base64-encoded images of the vehicle and license plate.

2. CARS.API processes the received request and decodes image into a format available for processing, and sequentially runs each passed classifier. 

3. After the end of the work of all classifiers CARS.API sends the response in JSON format back to the client. If the passed name of the classifier does not match any of the existing ones in the system, the corresponding message will be returned.

To get information about the vehicle and license plate, you need to send a request in JSON format to CARS.API.

The request body must contain certain fields, the list of fields and their description is presented in Table 1.

**Table 1**. Fields for forming a request

+-------------------+-------------------------------------------------------------------+
|**Field name**     |**Description**                                                    |
+===================+===================================================================+
|photo_ts           |An enlarged image of vehicle, encoded in base64.                   |
+-------------------+-------------------------------------------------------------------+
|photo_grz          |An enlarged image of license plate, encoded in base64.             |
+-------------------+-------------------------------------------------------------------+
|Classifiers        |Classifiers that contain fields with data about the vehicle and    |
|                   |                                                                   |
|                   |license plate and an assessment                                    |
+-------------------+-------------------------------------------------------------------+

1.1. Available Classifiers
--------------------------------------

Information about the vehicle or license plate is determined by classifiers. The full list of classifiers and their descriptions is in Table 2

**Table 2**. Description of classifiers
                                                                                        
+-------------------------------+-------------------------------------------------------+
|**Field name**                 |**Description**                                        |
+===============================+=======================================================+
|grz_ai_recognition             |License plate recognition. Returns the recognized      |
|                               |                                                       |
|                               |license plate symbols (series, registration number and |
|                               |                                                       |
|                               |registration region code) and an estimate of accuracy  |
|                               |                                                       |
|                               |of recognition of each of the license plate signs.     |
+-------------------------------+-------------------------------------------------------+
|marka_taxi_mt                  |Returns the brand of the vehicle and its belonging to  |
|                               |                                                       |
|                               |the public transport.                                  |
+-------------------------------+-------------------------------------------------------+
|pmt_grz_quality                |Evaluates the quality of the license plate image.      |
+-------------------------------+-------------------------------------------------------+
|pmt_bad_photo                  |Evaluates the image quality of the vehicle.            | 
+-------------------------------+-------------------------------------------------------+
|ts_bcd_type                    |Defines the vehicle type.                              |
+-------------------------------+-------------------------------------------------------+
|solid_line_intersection        |IDetermines if the car has violated the prohibition on |
|                               |                                                       |
|                               |crossing a solid line.                                 |
+-------------------------------+-------------------------------------------------------+
|speed_bad_good_spec            |In case of violation of the speed limit recorded by the|
|                               |                                                       |
|                               |camera, determines the conditions for issuing a fine.  |
|                               |                                                       |
|                               |A fine is not issued in following cases:               |
|                               |                                                       |
|                               | - vehicle belongs to a special vehicle,;              |
|                               |                                                       |
|                               | - the image has a low quality.                        |
+-------------------------------+-------------------------------------------------------+
|car_brand_model                |Defines the brand and model of the vehicle.            |
+-------------------------------+-------------------------------------------------------+
|vehicle_color                  |Determines the color of the vehicle.                   |
+-------------------------------+-------------------------------------------------------+
|vehicle_type                   |Defines type of vehicle.                               |
+-------------------------------+-------------------------------------------------------+
|vehicle_descriptor             |Retrieves vehicle descriptor.                          |
+-------------------------------+-------------------------------------------------------+
|grz_country_recognition_v1     |Returns country of license plate registration.         |
+-------------------------------+-------------------------------------------------------+
|eu_recognition_v1              |Returns results of the license plate recognition of EU.|
+-------------------------------+-------------------------------------------------------+
|rus_spec_recognition_v1        |Returns results of emergency vehicle recognition       |
|                               |                                                       |
|                               |numbers in Russia.                                     |    
+-------------------------------+-------------------------------------------------------+
|grz_bel_ukr_kzh_recognition_v1 |It is used for recognizing the license plate of        |
|                               |                                                       |
|                               |Belarus, Ukraine, and Kazakhstan.                      |
+-------------------------------+-------------------------------------------------------+
|grz_all_countries              |It is used to determine whether the license plate      |
|                               |                                                       |
|                               |belongs to a country, and then launches the recognition|
|                               |                                                       |
|                               |classifier for the corresponding country.              |    
+-------------------------------+-------------------------------------------------------+
|uae_recognition_v1             |The classifier is used to recognize UAE license plate. |
+-------------------------------+-------------------------------------------------------+
|grz_emirate_recognition_v1     |The classifier returns emirate of the license plate.   |
+-------------------------------+-------------------------------------------------------+
|grz_ai_recognition             |It is used to determine whether the license plate      |
|                               |                                                       |
|                               |belongs to a country, and then launches the recognition|
|                               |                                                       |
|                               | classifier for the corresponding country. Unlike      |
|                               |                                                       |
|                               |grs_all_countries, it does not output the name and     |
|                               |                                                       |
|                               |accuracy of a specific country.                        |
+-------------------------------+-------------------------------------------------------+
|vehicle_emergency_type         |Determines the type of emergency service based on the  |
|                               |                                                       |
|                               |vehicle image.                                         |
+-------------------------------+-------------------------------------------------------+

CARS API returns the response as a JSON object.

