User Manual
=========================

Glossary
--------------------------

+-------------------+--------------------------------------------------------------------+
|**Term**           | **Description**                                                    |
+===================+====================================================================+
|Classifier         |A system parameter that recognizes one of the vehicle or license    |
|                   |                                                                    |
|                   |plate attributes.                                                   |
+-------------------+--------------------------------------------------------------------+
|EXIF               |A format that allows you to add additional information (metadata) to|
|                   |                                                                    |
|                   | images, commenting on this file, describing the conditions and     |
|                   |                                                                    |
|                   |methods of obtaining it, authorship, etc.                           |
+-------------------+--------------------------------------------------------------------+
|LP (License plate) |A vehicle registration plate.                                       |
+-------------------+--------------------------------------------------------------------+

Introduction
----------------------

This document is manual for user of the CARS API 0.0.12 service.

The manual defines how the user operates in CARS.API service.

It is recommended to carefully read this manual before using the service.

General Information
-----------------------------

**VisionLabs LUNA CARS** is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS.Analytics, CARS.API and CARS.Stream.

**VisionLabs LUNA CARS API** is a vehicle recognition service that allows in real-time following actions:

* identify vehicle brands and models;

* detect license plate;

* determine whether the vehicle belongs to public transport, taxi, or emergency vehicles;

* evaluate the quality of incoming images;

* determine the conditions for issuing a fine for vehicles that have violated the speed limit;

* checking the intersection of a solid line;

* determine type of vehicle;

* determine color of vehicle;

* extract the vehicle descriptor;

* determine country of vehicle registration.

System Requirements
-------------------------

Image Requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Requirements for incoming images of vehicles and LP:

* Images must be three-channel (RGB) or black and white;

* Image format: JPEG encoded in the Base64 standard;

* Images should not contain EXIF tags;

* The angle of shooting of the vehicle and the LP can be any except for «vertical», when the camera is above the object;

* Vehicle and license plate must be fully visible on the frame;

* Supported image size from 320x240 to 1920x1080 px.

Video Requirements
~~~~~~~~~~~~~~~~~~~~~~~

Requirements for incoming video of vehicles and LP:

* Recommended resolution - 1920x1080 px;

* The frame rate must be constant;

* Supported bitrate - 4096 Kb/s;

* Shutter speed (exposure) - no more than 1/200 ;

* Used data transfer protocols - TCP, RTCP.

.. note:: This set of parameters (except for exposure) is the recommended minimum at which the system works efficiently. Decreasing the values may also reduce the number of detections, while increasing the value may unnecessarily load the system.

(*) The shutter speed should be selected based on the speed of traffic. The higher the speed of traffic, the faster the shutter should operate.

1. Service Control Commands
----------------------------------

1.1. Service Request
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To obtain information about the vehicle and the license plate, it is necessary to send a request to the service.

This section describes the creation of requests in CARS.API and describes the content of the service response.

.. note:: Sending requests via the web-interface is presented in paragraph 2.3 of «CARS Analytics. Administration Manual». Data in API methods is transmitted in JSON format. The request is sent via the POST method to the URL /<request type>. The HTTP request header contains a key-value pair: Content-Type: application / json.

Example request using Curl: ::

    curl -k -v -X POST Content-Type: application/json -d @/request.req http://<IP_address>:8080/classify

1.1.1. Processing of One Pair of Images of Vehicle and LP
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Request to receive recognition results for one pair of vehicle and license plate images.

The request is sent using the POST method to the URL /classify.

The request body should contain the following fields:

* photo_grz – image of vehicle license plate in Base64 format

* photo_ts – vehicle image in Base64 format

* classifiers – an array of classifiers. Indicate the data about the vehicle and the license plate that you want to receive.

An example of a request to process one pair of images: ::

    photo_ts: <Base64 encoded contents of image of transport>,
    photo_grz: <Base64 encoded contents of image of transport plate>,    
    classifiers: [    
    classifier1,
    ... ]

There are two options for specifying classifiers:

* Specify only the required classifiers:

::

    classifiers : [
        grz_ai_recognition,
        marka_taxi_mt, ]

* Leave the array empty. In this case, all classifiers will be applied:

::

    classifiers : []

1.1.2. Result of Processing One Pair of Images of Vehicle and LP
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

After the images are loaded, they are processed. If a failure occurs at any stage of processing, the service returns a corresponding error.

Example of error message: ::

    400:
    description: Bad Request.
    body:
        application/json:
        example:
            {
                error: parse error - unexpected :; expected end of input
            }

If the processing was successful, then the service responds in the form of a JSON object. This object contains information obtained for each of the specified classifiers.

An example of an answer with all classifiers: ::

    200:
    {
        results: [
        {
            classifier: grz_ai_recognition,
            regno_ai: {
                scores: [
                0.998958945274353,
                0.999943137168884,
                0.999952435493469,
                0.999989628791809,
                0.999976396560669,
                0.999926209449768,
                0.999958872795105,
                0.601863384246826
                ],
            symbols: [
          Т,
          6,
          5,
          8,
          Т,
          Н,
          3,
          6
        ],
        length_scores: [
          0.0038961709942668676,
          0.0038927060086280107,
          0.0038938382640480995,
          0.003894105553627014,
          0.00389483361504972,
          0.0038959593512117863,
          0.0038981761317700148,
          0.007125549018383026,
          0.9546269536018372,
          0.00708411037921906,
          0.0038976280484348536
        ]
      },
      regno_ai_score: 0.60108345746994
    },
    {
      MT: 2.24701793172244e-7,
      classifier: marka_taxi_mt,
      model: МАЗ,
      model_score: 0.999993324279785,
      not_MT: 0.999998092651367,
      taxi: 0.00000167787061400304
    },
    {
      classifier: pmt_bad_photo,
      pmt_bad_photo_ts: 0.0469167605042458
    },
    {
      classifier: pmt_grz_quality,
      trash: 9.63512533647126e-11
    },
    {
      classifier: ts_bcd_type,
      ts_type_ai: B_light,
      ts_type_ai_score: 2.15372297773797e-9
    },
    {
      classifier: speed_bad_good_spec,
      code: 45,
      0_score: 3.32414605991377e-17,
      40_score: 1,
      45_score: 1
    },
    {
      classifier: solid_line_recognition,
      code: 0_39,
      0_39_score: 0.8868,
      38_score: 0.1132,
      intersection: true
     }
    ]
    }

1.1.3. Processing of Several Pairs of Images of Vehicle and LP
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

This request allows obtaining recognition results for several pairs of photographic images of vehicles and license plates. This type of request does not support the solid_line_intersection classifier (for more details about this classifier, see paragraph 2.6 of this manual). It is also necessary to explicitly indicate the list of requested classifiers.

The request is POST to the /bulk_classify URL.

The body of the request contains a photos object of type JSON containing the following fields:

* photo_grz – image of vehicle license plate in Base64 format;

* photo_ts – vehicle image in Base64 format;

* rect_ts – vehicle coordinates on the original photo image, on which the vehicle was detected;

* rect_grz – coordinates of the vehicle license plate on the original image, on which the license plate was detected;

* frame_id – image number;

* track_id – track number;

* cam_id – camera number.

The fields «rect_ts», «rect_grz», «frame_id», «track_id», «cam_id» are optional and can be omitted.

The body of the request also contains a «classifiers» object of JSON type, which is an array of classifiers (information that you want to get).

Sample batch request: ::
    
    post:
    description: Classify car characteristics on attached photo.
    body:
      application/json:
        example:
          {
            photos: [
              {
                photo_ts: <Base64 encoded contents of image of transport>,
                rect_ts: {w: 10, h: 10, x: 0, y: 0},
                photo_grz: <Base64 encoded contents of image of transport>,
                rect_grz: {w: 20, h: 20, x: 5, y: 7},
                cam_id: 0,
                frame_id: 0,
                track_id: 0
              }
            ],
              classifiers : [
                classifier1,
                classifier2,
                   ... ]
          }

1.1.4. Result of Processing Several Pairs of Images of Vehicle and LP 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

After the images are loaded, they are processed. If a failure occurs at any stage of processing, the service returns an error message.

Sample error message: ::

    400:
        description: Bad Request.
        body:
          application/json:
            example:
              {
                error: parse error - unexpected :; expected end of input
              }

If the processing was successful, then the service returns a response in the form of a JSON object. The «result» array contains the results of recognizing all objects in the photos array from the query.

An example of an answer with all classifiers: ::
      
    200:
        body:
          application/json:
            example:
              {
                 results:[
                    {
                       frame_id:0,
                       recognition_results:[
                          {
                             classifier:grz_ai_recognition,
                             regno_ai:{
                                length_scores:[
                                   0.0,
                                   0.0,
                                   0.0,
                                   0.0,
                                   0.0,
                                   0.0,
                                   0.0,
                                   0.0,
                                   1e-05,
                                   0.99999,
                                   0.0
                                ],
                                scores:[
                                   0.99995,
                                   0.99997,
                                   0.99994,
                                   0.99992,
                                   0.99998,
                                   0.99992,
                                   0.99999,
                                   0.99999,
                                   0.99998
                                ],
                                symbols:[
                                   Х,
                                   5,
                                   3,
                                   7,
                                   С,
                                   О,
                                   7,
                                   7,
                                   7
                                ]
                             },
                             regno_ai_score:0.99965
                          },
                          {
                             MT:0.0,
                             classifier:marka_taxi_mt,
                             model:СУЗУКИ,
                             model_score:0.99595,
                             not_MT:0.99869,
                             taxi:0.00131
                          },
                          {
                             classifier:pmt_bad_photo,
                             pmt_bad_photo_ts:0.01141
                          },
                          {
                             classifier:pmt_grz_quality,
                             trash:0.0
                          },
                          {
                             0_score:1.0,
                             40_score:0.0,
                             45_score:0.0,
                             classifier:speed_bad_good_spec,
                             code:0
                          },
                          {
                             classifier:ts_bcd_type,
                             ts_type_ai:B_light,
                             ts_type_ai_score:1.0
                          }
                       ],
                       track_id:0  
                       } 
                    ] 
                }

1.1.5. Request for Vehicle and LP Detection
""""""""""""""""""""""""""""""""""""""""""""""""""""

This request allows you to receive bbox TS and GRZ. This request type supports the «car» and «grz» detectors.

The request is sent by the POST method to the URL /detector.

The request body contains a JSON body object containing the following fields:

* photo – vehicle image in Base64 format

* detectors – list of detectors

* max_detection_count – maximum number of detections per photo

Request example: ::

    post:
     description: Classify car characteristics on attached photo.
        body:
        application/json:
        example:
          {
          photo: base64 image,
           detectors:
              [{name: car, score_threshold: 0.3},
               {name: grz, score_threshold: 0.5},
               {name: grz_123}]
           max_detection_count: 10
           }

1.1.6. Service Response
""""""""""""""""""""""""""""""""

After the images are loaded, they are processed. If a failure occurs at any stage of processing, the service returns an error.

Error message: ::

    400:
        description: Bad Request.
        body:
          application/json:
            example:
              {
                error: parse error - unexpected :; expected end of input
              }

If the processing was successful, then the service returns a response in the form of a JSON object. The results array contains the results of detecting all objects in the detectors array from the query.

Response: ::

    200:
    body:
    application/json:
      {results:
        [{detections:
          [{height: 198, score: 0.99878, width: 202, x: 299, y: 4},
          {height: 206, score: 0.99567, width: 256, x: 253, y: 291},
          {height: 112, score: 0.99499, width: 158, x: 591, y: 0},
          {height: 242, score: 0.9631, width: 301, x: 769, y: 424}],
        detector: car, score_threshold: 0.3},
        {detections:
          [{height: 27, score: 0.7581, width: 73, x: 895, y: 584},
          {height: 28, score: 0.72786, width: 79, x: 339, y: 415}],
        detector: grz, score_threshold: 0.5},
        {detector: grz_123, error: unknown detector}]}

2. Classifiers
------------------------

In the section, classifiers available in the service and their fields are defined. Depending on the «useUAEClassifiers» parameter values (recognition of UAE vehicle numbers), classifiers are available according to the following rules, presented in Table 1.

**Table 1**. Available classifiers

+---------------------------+-------------------------------+------------------------------+
|Available anytime          |Available if                   |Available if                  |
|                           |                               |                              |
|                           |useUAEClassifiers: true        |useUAEClassifiers: false      |
+===========================+===============================+==============================+
|marka_taxi_mt              |uae_recognition_v1             |grz_country_recognition_v1    |
+---------------------------+-------------------------------+------------------------------+
|pmt_grz_quality            |grz_emirate_recognition_v1     |grz_ai_recognition_v2         |
+---------------------------+-------------------------------+------------------------------+
|pmt_bad_photo              |                               |eu_recognition_v1             |
+---------------------------+-------------------------------+------------------------------+
|ts_bcd_type                |                               |rus_spec_recognition_v1       |
+---------------------------+-------------------------------+------------------------------+
|solid_line_intersection    |                               |grz_bel_ukr_kzh_recognition_v1|
+---------------------------+-------------------------------+------------------------------+
|speed_bad_good_spec        |                               |                              |
+---------------------------+-------------------------------+------------------------------+
|car_brand_model            |                               |                              |
+---------------------------+-------------------------------+------------------------------+
|vehicle_color              |                               |                              |
+---------------------------+-------------------------------+------------------------------+
|vehicle_type               |                               |                              |
+---------------------------+-------------------------------+------------------------------+    
|vehicle_emergency_type     |                               |                              |
+---------------------------+-------------------------------+------------------------------+
|vehicle_descriptor         |                               |                              |
+---------------------------+-------------------------------+------------------------------+
|grz_all_countries          |                               |                              |
+---------------------------+-------------------------------+------------------------------+
|grz_ai_recognition         |                               |                              |
+---------------------------+-------------------------------+------------------------------+

2.1. Classifier «grz_ai_recognition_v2»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «grz_ai_recognition_v2» is used to recognize the license plate of vehicles of the Russian Federation.

The «recogn_ai» parameter includes the license plate recognition results. Contains the following datasets, which are described in Тable 2.

**Table 2**. Description of fields

+-------------------+-------------+--------------------------------------------------------+
|Field name         |Type         |Description                                             |
+===================+=============+========================================================+
|length_scores      |float        |An array displaying the number of license plates. There |
|                   |             |                                                        |
|                   |             |are 11 elements in the array:                           |
|                   |             |                                                        |
|                   |             |The first element of array corresponds to the LP with 0 |
|                   |             |                                                        |
|                   |             |characters, The eleventh element – 10 characters.       |
|                   |             |                                                        |
|                   |             |Each element contains a number that shows probability of|
|                   |             |                                                        |
|                   |             |the presence in the license plate of exactly this number|
|                   |             |                                                        |
|                   |             |of characters.                                          |
|                   |             |                                                        |
|                   |             |If 10 array elements have the highest value, then system|
|                   |             |                                                        |
|                   |             |has identified 9 characters in the license plate        |
|                   |             |                                                        |
|                   |             |The sum of the values of all 11 elements is 1.          |
+-------------------+-------------+--------------------------------------------------------+
|scores             |float        |An array displaying the assessment of the recognition   |
|                   |             |                                                        |
|                   |             |accuracy of each license plate symbol.                  |
+-------------------+-------------+--------------------------------------------------------+
|symbols            |string       |Array of recognized LP. Recognizable characters include |
|                   |             |                                                        |
|                   |             |series, registration number and registration region code|
+-------------------+-------------+--------------------------------------------------------+
|regno_ai_score     |float        |General assessment of the recognition accuracy of the LP|
+-------------------+-------------+--------------------------------------------------------+

Response example: ::

    {
    classifier: grz_ai_recognition,
    regno_ai: {
        length_scores: [
            0.0005,
            0.0005,
            0.0005,
            0.0005,
            0.0006,
            0.0005,
            0.0026,
            0.0017,
            0.9833,
            0.0013,
            0.0005,
            0.0015,
            0.0015,
            0.0015,
            0.0015,
            0.0015
        ],
        scores: [
            0.9257,
            0.4536,
            0.5579,
            0.2742,
            0.179,
            0.1607,
            0.7307,
            0.5866
        ],
        symbols: [
            Т,
            Е,
            4,
            0,
            7,
            8,
            А,
            А 
        ]
        },
    regno_ai_score: 0.0008 
    }

2.2. Classifier «marka_taxi_mt»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «marka_taxi_mt» returns the brand of the vehicle and checks if vehicle is public transport. The description of the fields is presented in the Table 3.

**Table 3**. Description of fields

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|model              |string   |Vehicle brand                                               |
+-------------------+---------+------------------------------------------------------------+
|model_score        |float    |Assessment of the recognition accuracy of the car brand     |
+-------------------+---------+------------------------------------------------------------+
|MT                 |float    |Assessment of vehicle belonging to route transport          |
+-------------------+---------+------------------------------------------------------------+
|not_MT             |float    |Assessment of the fact that vehicle does not belong to the  |
|                   |         |                                                            |
|                   |         |public transport.                                           |  
+-------------------+---------+------------------------------------------------------------+
|taxi               |float    |Assessment that the vehicle is a taxi.                      |
+-------------------+---------+------------------------------------------------------------+

Response example: ::

    {
        classifier: marka_taxi_mt,
        model: ШКОДА,
        model_score: 0.999976277351379,
        MT: 0.00000933460887608817,
        not_MT: 0.00118051946628839,
        taxi: 0.99881017208099
    }

2.3. Classifier «pmt_grz_quality»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «pmt_grz_quality» evaluates the image quality of the license plate. The image quality is determined based on the data of the image area. The classifier parameters are described in Table 4.

The image quality is assessed by a special neural network. The quality assessment is based on the following criteria:

* overexposure;

* lack of light;

* blur and lack of sharpness.

**Table 4**. Description of fields

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|trash              |float    |Evaluation of the quality of LP image. The value is from 0  |
|                   |         |                                                            |
|                   |         |to 1. The higher value of the parameter means lower quality |
|                   |         |                                                            |
|                   |         |of the photo.                                               |
+-------------------+---------+------------------------------------------------------------+

Response example: ::

    {  
        classifier: pmt_grz_quality,
        trash: 9.63512533647126e-11 
    }

2.4. Classifier «pmt_bad_photo»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «pmt_bad_photo» evaluates the image quality of the vehicle. The classifier parameters are described in the Table 5.

**Table 5**. Description of fields

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|pmt_bad_photo_ts   |float    |Assessment of the quality of the enlarged image of vehicle. |
|                   |         |                                                            |
|                   |         |The value is from 0 to 1. The higher value of the parameter |
|                   |         |                                                            |
|                   |         |means lower the quality of the photo.                       |
+-------------------+---------+------------------------------------------------------------+

Response example: ::
    
    {
        classifier: pmt_bad_photo,
        pmt_bad_photo_ts: 0.0469167605042458
    }

2.5. Classifier «ts_bcd_type»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «ts_bcd_type» determines the type of vehicle. The classifier parameters are described in the Table 6.

**Table 6**. Description of fields

+------------+-----------------------------------------------------------------------------+
|**Field**   |**Decsription**                                                              |
+============+=============================================================================+
|ts_type_ai  |accuracy of determining the vehicle category                                 |
+------------+-----------------------------------------------------------------------------+
|B_light     |vehicle type                                                                 |
+------------+-----------------------------------------------------------------------------+
|B_heavy     |В, small trucks                                                              |
+------------+-----------------------------------------------------------------------------+
|C_light     |С, truck with a load capacity up to 12 tons                                  |
+------------+-----------------------------------------------------------------------------+
|C_heavy     |С, truck with a load capacity more than 12 tons                              |
+------------+-----------------------------------------------------------------------------+
|D_light     |D, buses with one-piece body                                                 |
+------------+-----------------------------------------------------------------------------+
|D_long      |D, articulated buses                                                         |
+------------+-----------------------------------------------------------------------------+

Response example: ::

    {  
        classifier: ts_bcd_type,
        ts_type_ai: B_light,
        ts_type_ai_score: 1 
    }

2.6. Classifier «solid_line_intersection»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «solid_line_intersection» determines if the vehicle has violated the ban on crossing the solid line. Decision is made to issue a fine based on this information. To use this classifier, you need a «trace» of vehicle tracking in the form of a yellow line from the complex for photo fixation of the violation. The classifier fields are described in the Table 8.

**Table 8**. Description of parameters

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|code               |string   |Result code of solid line intersection                      |
+-------------------+---------+------------------------------------------------------------+
|0_39_score         |float    |Solid line is not visible                                   |
+-------------------+---------+------------------------------------------------------------+
|38_score           |float    |The presence of a solid line on the road                    |
+-------------------+---------+------------------------------------------------------------+
|intersection       |float    |Intersection detection                                      |
+-------------------+---------+------------------------------------------------------------+

Response example: ::

    { 
        classifier: solid_line_intersection,
        code: 0_39,
        0_39_score: 0.8868,
        38_score: 0.1132,
        intersection: true
    }

2.7. Classifier «speed_bad_good_spec»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «speed_bad_good_spec» defines a speed violation code. CARS API does not measure speed. The camera records the fact of violation of the speed limit. CARS.API determines the image quality and the type of vehicle for which the violation was detected. Based on this information, a decision can be made to issue a fine. The classifier fields are described in the Table 9.

**Table 9**. Description of fields

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|code               |string   |speed violations result code                                |
+-------------------+---------+------------------------------------------------------------+
|0_score            |float    |a fine for violation of the speed limit is issued           |
+-------------------+---------+------------------------------------------------------------+
|40_score           |float    |poor quality photo, no fine                                 |
+-------------------+---------+------------------------------------------------------------+
|45_score           |float    |The vehicle is a special vehicle, a fine is not issued      |
+-------------------+---------+------------------------------------------------------------+

Response example: ::

    {
        classifier: speed_bad_good_spec,
        code: 45,
        0_score: 3.32414605991377e-17,
        40_score: 1,
        45_score: 1 
    }

2.8. Classifier «car_brand_model»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «car_brand_model» defines the brand and model of the vehicle. The classifier fields are described in Table 10.

**Table 10**. Description of fields

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|brand              |string   |vehicle brand                                               |
+-------------------+---------+------------------------------------------------------------+
|model              |string   |vehicle model                                               |
+-------------------+---------+------------------------------------------------------------+
|score              |float    |assessment of accuracy of determining vehicle brand or model|
+-------------------+---------+------------------------------------------------------------+

If the accuracy score for this classifier is below the scoreThreshold threshold (0.5 by default), which can be configured in the car_brand_model section of the ./data/modelRunner.cfg configuration file, then the result is considered unacceptable. In this case, the following response will be returned indicating the obtained accuracy score: ::

    {
        classifier: car_brand_model, brand: background, model: background, score: 0.80309
    }

2.9. Classifier «vehicle_color»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «vehicle_color» determines the color of the vehicle. The classifier fields are described in the Table 11.

**Table 11**. Description of parameters

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|vehicle_color      |string   |vehicle color                                               |
+-------------------+---------+------------------------------------------------------------+
|vehicle_color_score|float    |assessment of the accuracy of determining the vehicle color |
+-------------------+---------+------------------------------------------------------------+

There are 16 classes of colors defined in system. A description of each is presented in the Table 12.

**Table 12**. Service colors
                        
+-----------------------+-----------------------+
|**Field name**         |**Description**        |
+=======================+=======================+
|vehicle_color          |Color                  |                       
+-----------------------+-----------------------+
|beige                  |beige                  |         
+-----------------------+-----------------------+
|black                  |black                  |
+-----------------------+-----------------------+
|blue                   |blue                   |                       
+-----------------------+-----------------------+
|brown                  |brown                  |
+-----------------------+-----------------------+
|cherry                 |cherry                 |
+-----------------------+-----------------------+
|golden                 |golden                 |
+-----------------------+-----------------------+
|gray                   |gray                   |
+-----------------------+-----------------------+
|green                  |green                  |
+-----------------------+-----------------------+
|light_blue             |light_blue             |
+-----------------------+-----------------------+
|orange                 |orange                 |
+-----------------------+-----------------------+
|pink                   |pink                   |
+-----------------------+-----------------------+
|red                    |red                    |
+-----------------------+-----------------------+
|silver                 |silver                 |
+-----------------------+-----------------------+
|violet                 |violet                 |
+-----------------------+-----------------------+
|white                  |white                  |
+-----------------------+-----------------------+
|yellow                 |yellow                 |
+-----------------------+-----------------------+

Response example: ::

    {
        classifier: vehicle_color, vehicle_color: cherry, vehicle_color_score: 0.99993
    }

2.10. Classifier «vehicle_type»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «vehicle_type» defines the vehicle category. The classifier fields are described in the Table 13.

**Table 13**. Description of fields

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|ts_type_ai         |string   |Vehicle type                                                |
+-------------------+---------+------------------------------------------------------------+
|ts_type_ai_score   |float    |assessment of the accuracy of determining the vehicle type  |
+-------------------+---------+------------------------------------------------------------+

There are 5 types of vehicles. A description of each type is presented in the Table 14.

**Table 14**. Vehicle Types

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|ts_type_ai         |Type     |                                                            |
+-------------------+---------+------------------------------------------------------------+
|A                  |Type А   |Motorcycles                                                 |
+-------------------+---------+------------------------------------------------------------+
|B                  |Type B   |Passenger vehicles                                          |
+-------------------+---------+------------------------------------------------------------+
|C                  |Type C   |Trucks                                                      |
+-------------------+---------+------------------------------------------------------------+
|D                  |Type D   |Public transport                                            |
+-------------------+---------+------------------------------------------------------------+
|E                  |Type E   |Trucks with heavy load trailer (more than 750 kg)           |
+-------------------+---------+------------------------------------------------------------+

Response example: ::

    {
        classifier: vehicle_type, ts_type_ai: D, ts_type_ai_score: 0.99971
    }

2.11. Classifier «vehicle_emergency_type»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «vehicle_emergency_type» determines the type of emergency transport based on the vehicle photo. The classifier fields are described in the Table 15.

**Table 15**. Description of field

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|ts_type_ai         |string   |emergency transport category                                |
+-------------------+---------+------------------------------------------------------------+
|ts_type_ai_score   |float    |type determination accuracy assessment                      |
+-------------------+---------+------------------------------------------------------------+

The system has 5 categories of emergency transport. Each is described in Table 16.

**Table 16**. Emergency transport.

+--------------+----------------------------------------------------------------------------+
|**Field name**|**Description**                                                             |
+==============+============================================================================+
|ts_type_ai    |Emergency transport type                                                    |
+--------------+----------------------------------------------------------------------------+
|01            |Fire department                                                             |
+--------------+----------------------------------------------------------------------------+
|02            |Police                                                                      |
+--------------+----------------------------------------------------------------------------+
|03            |Ambulance                                                                   |
+--------------+----------------------------------------------------------------------------+
|112           |Emergency                                                                   |
+--------------+----------------------------------------------------------------------------+
|not_emergency |Not emergency                                                               |
+--------------+----------------------------------------------------------------------------+

Response example: ::

    {
        classifier: vehicle_emergency_type, ts_type_ai: 01, ts_type_ai_score: 0.99
    }

2.12. Classifier «vehicle_descriptor»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «vehicle_descriptor» retrieves vehicles descriptor.

The descriptor is a composite vector of vehicle photo image features. Image encoded inBase64 format. The descriptor is needed for comparison and search of the TS. The classifier fields are described in the Table 17.

**Table 17**. Description of fields

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|vehicle_descriptor |string   |vehicle descriptor                                          |
+-------------------+---------+------------------------------------------------------------+

2.13. Classifier «grz_country_recognition_v1»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «grz_country_recognition_v1» returns the country of the license plate registration. The classifier fields are described in the Table 18.

**Table 18**. Description of fields

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|country_score      |float    |assessment of the accuracy of determining the country       |
+-------------------+---------+------------------------------------------------------------+
|country            |string   |recognition result                                          |
+-------------------+---------+------------------------------------------------------------+

There are 5 designated countries of registration, as well as composite values. The classifier fields are described in the Table 19.

**Table 19**. County descriptions

+-------------+-----------------------------------------------------------------------------+
|**Name**     |**Description**                                                              |
+=============+=============================================================================+
|Country      |country                                                                      |
+-------------+-----------------------------------------------------------------------------+
|RUS          |Russia                                                                       |
+-------------+-----------------------------------------------------------------------------+
|KZH          |Kazakhstan                                                                   |
+-------------+-----------------------------------------------------------------------------+
|UKR          |Ukraine                                                                      |
+-------------+-----------------------------------------------------------------------------+
|BEL          |Belarus                                                                      |
+-------------+-----------------------------------------------------------------------------+
|EU           |European Union                                                               |
+-------------+-----------------------------------------------------------------------------+
|RUS_SPEC     |Russian special transport (police, diplomatic LP, military LP)               |
+-------------+-----------------------------------------------------------------------------+
|RUS_SQUARE   |Russian square LP (288x205 or 245x185 mm)                                    |
+-------------+-----------------------------------------------------------------------------+

Response example: ::

    {
        classifier:  grz_country_recognition_v1,
        country: RUS,
        country_score : 0.999976277351379
    }

2.14. Classifier «eu_recognition_v1»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «eu_recognition_v1» is used to recognize the license plates of the EU countries.

regno_ai enables license plate recognition results. Fields described in paragraph 2.1.

2.15. Classifier «rus_spec_recognition_v1»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «rus_spec_recognition_v1U+0022» is used to recognize the license plate for special vehicles of the Russian Federation as well as square numbers (288x205 or 245x185 mm). 

Parameter «regno_ai» enables license plate recognition results. Fields described in paragraph 2.1.

2.16. Classifier «grz_bel_ukr_kzh_recognition_v1»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «grz_bel_ukr_kzh_recognition_v1» is used to recognize the LP of Belarus, Ukraine, Kazakhstan.

Parameter «regno_ai» enables license plate recognition results. Fields described in paragraph 2.1.

2.17. Classifier «grz_all_countries»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «grz_all_countries» is used to determine whether the license plate belongs to a country, and then runs the recognition classifier for the corresponding country. The classifier fields are described in the Table 20.

**Table 20**. Description of parameters

+-------------------+-------------+--------------------------------------------------------+
|**Field name**     |**Type**     |**Description**                                         |
+===================+=============+========================================================+
|country_score      |float        |Assessment of the accuracy of determining the country   |
+-------------------+-------------+--------------------------------------------------------+
|country            |string       |Recognition result                                      |
+-------------------+-------------+--------------------------------------------------------+
|scores             |float        |An array displaying the assessment of the recognition   |
|                   |             |                                                        |
|                   |             |accuracy of each license plate symbol                   |
+-------------------+-------------+--------------------------------------------------------+
|symbols            |string       |Array of recognized license plates. Recognizable        |
|                   |             |                                                        |
|                   |             |characters include series, registration number and      |
|                   |             |                                                        |
|                   |             |registration region code                                |
+-------------------+-------------+--------------------------------------------------------+
|length_scores      |float        |An array displaying the number of license plates. There |
|                   |             |                                                        |
|                   |             |are 11 elements in the array:                           |
|                   |             |                                                        |
|                   |             |The first element of array corresponds to the LP with 0 |
|                   |             |                                                        |
|                   |             |characters, The eleventh element – 10 characters.       |
|                   |             |                                                        |
|                   |             |Each element contains a number that shows probability of|
|                   |             |                                                        |
|                   |             |the presence in the license plate of exactly this number|
|                   |             |                                                        |
|                   |             |of characters.                                          |
|                   |             |                                                        |
|                   |             |If 10 array elements have the highest value, then system|
|                   |             |                                                        |
|                   |             |has identified 9 characters in the license plate        |
|                   |             |                                                        |
|                   |             |The sum of the values of all 11 elements is 1.          |
+-------------------+-------------+--------------------------------------------------------+
|regno_ai_score     |float        |General assessment of the recognition accuracy of the LP|
+-------------------+-------------+--------------------------------------------------------+

2.18. Classifier «grz_ai_recognition»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «grz_ai_recognition» is used to determine whether a license plate belongs to a country, and then runs the recognition classifier for the corresponding country. Works similarly to grz_all_countries but does not display country information. This classifier has an advantage in work and response speed. The classifier fields are described in the Table 21.

**Table 21**. Description of fields

+-------------------+-------------+--------------------------------------------------------+
|**Field name**     |**Type**     |**Description**                                         |
+===================+=============+========================================================+
|scores             |float        |An array displaying the assessment of the recognition   |
|                   |             |                                                        |
|                   |             |accuracy of each license plate symbol                   |
+-------------------+-------------+--------------------------------------------------------+
|symbols            |string       |Array of recognized license plates. Recognizable        |
|                   |             |                                                        |
|                   |             |characters include series, registration number and      |
|                   |             |                                                        |
|                   |             |registration region code                                |
+-------------------+-------------+--------------------------------------------------------+
|length_scores      |float        |An array displaying the number of license plates. There |
|                   |             |                                                        |
|                   |             |are 11 elements in the array:                           |
|                   |             |                                                        |
|                   |             |The first element of array corresponds to the LP with 0 |
|                   |             |                                                        |
|                   |             |characters, The eleventh element – 10 characters.       |
|                   |             |                                                        |
|                   |             |Each element contains a number that shows probability of|
|                   |             |                                                        |
|                   |             |the presence in the license plate of exactly this number|
|                   |             |                                                        |
|                   |             |of characters.                                          |
|                   |             |                                                        |
|                   |             |If 10 array elements have the highest value, then system|
|                   |             |                                                        |
|                   |             |has identified 9 characters in the license plate        |
|                   |             |                                                        |
|                   |             |The sum of the values of all 11 elements is 1.          |
+-------------------+-------------+--------------------------------------------------------+
|regno_ai_score     |float        |General assessment of the recognition accuracy of the LP|
+-------------------+-------------+--------------------------------------------------------+

2.19. Classifier «uae_recognition_v1»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «uae_recognition_v1» used to recognize the UAE license plate. 

Parameter «regno_ai» enables license plate recognition results. Fields described in paragraph 2.1.

2.20. Classifier «grz_emirate_recognition_v1»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Classifier «grz_emirate_recognition_v1» returns the emirate of the license plate of the vehicle. The classifier fields are described in the Table 22.

**Table 22**. Description of fields

+-------------------+---------+------------------------------------------------------------+
|**Field name**     |**Type** |**Description**                                             |
+===================+=========+============================================================+
|country_score      |float    |assessment of the accuracy of determining the emirate       |
+-------------------+---------+------------------------------------------------------------+
|emirate            |string   |recognition result                                          |
+-------------------+---------+------------------------------------------------------------+

There are the following designated Emirates: DUBAI, ABU_DHABI, SHARJAH, KHAIMAN, QUWAIN, AJMAN, FUJAIRAH.

Response example: ::

    {  
        classifier:  grz_emirate_recognition_v1,
        emirate: DUBAI,
        country_score : 0.999976277351379
    }
