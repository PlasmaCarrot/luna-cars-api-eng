Administration Manual
=================================

Glossary
-----------------

+-------------------+--------------------------------------------------------------------+
|**Term**           | **Description**                                                    |
+===================+====================================================================+
|Bbox (Bounding box)|Rectangle that restricts the image with the detected object (vehicle|
|                   |                                                                    |
|                   |or license plate)                                                   |
+-------------------+--------------------------------------------------------------------+
|EXIF               |A format that allows you to add additional information (metadata) to|
|                   |                                                                    |
|                   | images, commenting on this file, describing the conditions and     |
|                   |                                                                    |
|                   |methods of obtaining it, authorship, etc.                           |
+-------------------+--------------------------------------------------------------------+
|LP (License plate) |A vehicle registration plate.                                       |
+-------------------+--------------------------------------------------------------------+

Introduction
----------------

This document is manual for administrator of the CARS API 0.0.12 service.

This manual describes the installation, configuration and administration of the service.

It is recommended to carefully read this manual before installing and using the service.

General Information
-----------------------

**VisionLabs LUNA CARS** is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS.Analytics, CARS.API and CARS.Stream.

**VisionLabs LUNA CARS API** is a vehicle recognition service that allows in real-time following actions:

* identify vehicle brands and models;

* detect license plate;

* determine whether the vehicle belongs to public transport, taxi, or emergency vehicles;

* evaluate the quality of incoming images;

* determine the conditions for issuing a fine for vehicles that have violated the speed limit;

* checking the intersection of a solid line;

* determine type of vehicle;

* determine color of vehicle;

* extract the vehicle descriptor;

* determine country of vehicle registration.

System Requirements
-------------------------

Several requirements and conditions must be considered when preparing the installation and operation of the CARS.API service. The list of system requirements is presented in Table 1.

**Table 1**. System Requirements
                                                                                        
+---------------------------+-----------------------------------------------------------+
|**Resource**               |**Recommended**                                            |
+===========================+===========================================================+
|CPU                        |Intel 4 Cores 2,0 GHz                                      |                        
+---------------------------+-----------------------------------------------------------+
|RAM                        |8 GB or higher                                             |         
+---------------------------+-----------------------------------------------------------+
|HDD or SSD                 |10 GB or higher                                            | 
+---------------------------+-----------------------------------------------------------+
|OS                         |CentOS 7.4 x86_64 *                                        |                       
+---------------------------+-----------------------------------------------------------+
|Supported instructions     |AVX 2                                                      |
+---------------------------+-----------------------------------------------------------+
|Software                   |Python 3.7;                                                |
|                           |                                                           |    
|                           |Nginx;                                                     |    
|                           |                                                           |    
|                           |Ansible-playbook;                                          |    
+---------------------------+-----------------------------------------------------------+
|Browser                    |Microsoft Edge (44.0 and up);                              |
|                           |                                                           |    
|                           |Mozilla Firefox (60.3.0 and up);                           |    
|                           |                                                           |    
|                           |Google Chrome (50.0 and up).                               |    
+---------------------------+-----------------------------------------------------------+

* Any other similar OS with Python 3.7 support can use as an operating system (when deploying in Docker).

If you are using the resources of the graphic card NVIDIA graphics cards with support for CUDA 10.1 and at least 2 GB of video memory are supported. Pascal, Volta, Turning architectures are supported.

The above configuration will ensure the operation of the software package for demonstration purposes and is not intended for commercial systems. The commercial configuration is calculated based on the expected system load. Server performance requirements depend on the number of requests per unit of time at a normalized response time.
 
Image Requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Requirements for incoming images of vehicles and LP:

* Images must be three-channel (RGB) or black and white;

* Image format: JPEG encoded in the Base64 standard;

* Images should not contain EXIF tags;

* The angle of shooting of the vehicle and the LP can be any except for «vertical», when the camera is above the object;

* Vehicle and license plate must be fully visible on the frame;

* Supported image size from 320x240 to 1920x1080 px.

Video Requirements
~~~~~~~~~~~~~~~~~~~~~~~

Requirements for incoming video of vehicles and LP:

* Recommended resolution - 1920x1080 px;

* The frame rate must be constant;

* Supported bitrate - 4096 Kb/s;

* Shutter speed (exposure) - no more than 1/200 ;

* Used data transfer protocols - TCP, RTCP.

.. note:: This set of parameters (except for exposure) is the recommended minimum at which the system works efficiently. Decreasing the values may also reduce the number of detections, while increasing the value may unnecessarily load the system.

(*) The shutter speed should be selected based on the speed of traffic. The higher the speed of traffic, the faster the shutter should operate.

1. Installation
------------------

1.1. Pre-install Process
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The CARS API distribution is a «luna-cars_v.*.zip» archive.

The archive contains the components necessary for the installation and operation of the service. The archive does not include additional dependencies that are included in the standard distribution of the CentOS repository and can be downloaded from open sources during the installation process.

.. note::  Entire configuration and installation process must be performed under a superuser account (with root rights).

Before installing, place the distribution files and installation scripts in a separate folder on the server from which the installation will be performed. This folder should not contain any other distribution and license files other than the target files used to install the final product.

Installation via Ansible and Docker takes place in conjunction with the CARS.Analytics distribution.

1.2. Switching between AVX and GPU
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CARS.API can use a video card or processor for calculations. For each of these architectures, the distribution includes models with the corresponding postfix (_gpu.plan, _сpu.plan).

You can check AVX2 support by running the command: ::

    lscpu | grep -o avx2

To select the architecture and set its settings, use configuration file /data/modelRunner.cfg. Description of file parameters is presented in Table 2.

**Table 2**. Parameters of «modelRunner.cfg» configuration file 

+---+------------------+--------------------------------------------------------------------+
|#  |**Parameter**     | **Description**                                                    |
+===+==================+====================================================================+
|1  |numThreads        |Number of processor threads on which the service starts.            |
+---+------------------+--------------------------------------------------------------------+
|2  |planType          |Architecture type. Possible values:                                 |
|   |                  |                                                                    |
|   |                  |cpu – CPU computation;                                              |
|   |                  |                                                                    |
|   |                  |gpu – GPU computation.                                              |
+---+------------------+--------------------------------------------------------------------+
|3  |gpuNumber         |Serial number of the used video card.                               |
+---+------------------+--------------------------------------------------------------------+
|4  |numComputeStreams |The number of allocated CUDA threads.                               |
+---+------------------+--------------------------------------------------------------------+

Recognition results of the «marka_taxi_mt» and «grz_ai_recognition_v2» classifiers can be represented using Latin or Cyrillic characters. «useLatinCharacters» parameter is used to choose characters language. Parameter is in configuration file data/modelRunner.cfg. Possible values:

* True – using Latin characters;

* False – using Cyrillic characters.

1.3. Installation with Ansible
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You need to install the Ansible package by running the commands.

Package manager update: ::

    yum update

Additional repositories install: ::

    yum install epel-release

Ansible installation: ::

    yum install ansible

1.3.1. Settings SSH
''''''''''''''''''''''''''''

On the source server, you need to generate an SSH-key and add it to the target server. First you need to check and configure the SSH-service: ::

    systemctl status sshd

Кun servise if SSH ended: ::

    systemctl start sshd

Install server if needed: ::
    
    yum install -y openssh-server

After that, you need to generate a key by running the command: ::

    ssh-keygen

If necessary, you can specify a passphrase or leave it blank. To copy the key to the target server, enter the command: ::

    ssh-copy-id username@hostname

«username» is the name of the authorized user and «hostname» is the IP-address of the target server.

.. note:: This method is not the only one possible. You can use any other convenient method to provide SSH bridge between the installer server and the target server.

1.3.2. Settings of «hosts» Configuration File
'''''''''''''''''''''''''''''''''''''''''''''''''''''

The archive contains the «hosts» configuration file. This file is in the /ansible directory. You need to set in this file the external IP-address of the server where the application will be installed and path to NGINX balancer location. ::
   
    #CARS API
    #Multiple hosts allowed
    [api]
    <IP_address

    #Only 1 host
    [nginx]
    <IP_address>

1.3.3. Settings of «all.yml» Configuration File
''''''''''''''''''''''''''''''''''''''''''''''''''''''

It is necessary to configure the service in the «all.yml» configuration file located in the /ansible/group_vars directory. Description of the parameters is in the Table 3.

**Table 3**. Parameters of «all.yml» configuration file 

+---+----------------------+----------------------------------------------------------------+
|#  |**Параметр**          | **Описание**                                                   |
+===+======================+================================================================+
|1  |luna_cars_vers        |Specifies the name of archive. For example, luna-cars_v.0.0.12. |
+---+----------------------+----------------------------------------------------------------+
|2  |luna_cars_zip_location|This parameter sets the path to the CARS.Stream archive.        |
|   |                      |                                                                |
|   |                      |For example, /distr/api/<luna-cars_v.0.0.12>.                   |
+---+----------------------+----------------------------------------------------------------+

1.3.4. Service Port Configuration
''''''''''''''''''''''''''''''''''''''''''''''''''''

Before installation, it is also necessary to configure the service ports that will be used during operation. This can be done in the port section.

.. note:: A list of all ports by default and their purpose is presented in Appendix 1.

It is recommended to leave the default port number. The «CARS_API_PORT_RANGE» variable allows you to set the number of ports starting from the port specified in the «CARS_API_PORT» variable.

During installation, a configuration file of the NGINX balancer will be automatically created in the /etc/nginx/conf.d/cars-api.conf directory, which will contain all ports and the rules for accessing them. An example of this file is presented below: ::

    upstream lunaCars {
        least_conn;
        server<IP_адрес>:8100;
        server<IP_адрес>:8101;
        keepalive 8; }
        server {
        client_max_body_size 100m;
        keepalive_timeout 600;
        proxy_connect_timeout 600s;
        proxy_read_timeout 600s;
        proxy_send_timeout 600s;
        listen 81 default_server;
        server_name _;
        location / {
        proxy_pass http://lunaCars; }
        }

1.3.5. Country Selection
'''''''''''''''''''''''''''

In the «all.yml» configuration file, you can select one of two countries in which the system will operate: ::

    #Will use Emirates GRZ classifiers (True)
    #Will use default GRZ classifiers (False)   
    EMIRATES: False

To select the license plate recognition mode, you need to change the value of the EMIRATES parameter:

* True – system will recognize only UAE license plate;

* False – system will recognize the license plates of other countries (RF, CIS, EU).


1.3.6. Запуск установки через Ansible
''''''''''''''''''''''''''''''''''''''''''

.. note:: During the installation process, the built-in firewall and selinux will be disabled (a reboot will be required in the future). In some cases, an error may appear stating that the repositories are unavailable, in which case you must restart the installation.

To start the installation, you must be in /ansible directory and execute the following command: ::

    ansible-playbook -i hosts install_api.yml

1.3.7. Service Check
''''''''''''''''''''''''''''''''''''''''

After installation, it is possible to check the status of the CARS.API service with the command: ::

    systemctl status luna-cars-api

If CARS API works correctly, the system should not display any error messages.

1.4. Installation with Docker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1.4.1. Installation Docker and Docker-Compose
''''''''''''''''''''''''''''''''''''''''''''''

You can use the official `instruction <https://docs.docker.com/engine/install/centos/>`_ for CentOS, as this instruction is the most up to date.

Additional dependencies: ::

    sudo yum install -y yum-utils

Add docker repository: ::

    sudo yum-config-manager \
        --add-repo \
        https://download.docker.com/linux/centos/docker-ce.repo

Docker installations: ::

    sudo yum install docker-ce docker-ce-cli containerd.io

Installation check: ::

    docker -v

Download docker-compose: ::

    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

Assigning an Executable File Property: ::
    
    sudo chmod +x /usr/local/bin/docker-compose

1.4.2. Settings of «.env» File
''''''''''''''''''''''''''''''''''''''''

Description of the parameters is in the Table 4.

**Table 4**. Parameters of «.env» file

+---+-------------------+-------------------------------------------------------------------+
|#  |**Parameter**      | **Description**                                                   |
+===+===================+===================================================================+
|1  |HASP_license_server|Specifies the path to the server to which the installer calls for  |
|   |                   |                                                                   |
|   |                   |an available network product license. If there is no network       |
|   |                   |                                                                   |
|   |                   |license, then it must be specified locally.                        |
+---+-------------------+-------------------------------------------------------------------+
|2  |HASP_wait_time     |Specifies the time to response when a request for an available     |
|   |                   |                                                                   |
|   |                   |license is sent to the server. Set in minutes.                     |
+---+-------------------+-------------------------------------------------------------------+
|3  |Emirates           |Selecting country for license plate recognition. Possible values   |
|   |                   |                                                                   |
|   |                   |* true – system will recognize only UAE license plate;             |
|   |                   |                                                                   |
|   |                   |* false – system will recognize license plates of RF, CIS and EU.  |
+---+-------------------+-------------------------------------------------------------------+
|4  |ENG                |Specifies service language. Possible values:                       |
|   |                   |                                                                   |
|   |                   |* true – English UI;                                               |
|   |                   |                                                                   |
|   |                   |* false – Russian UI.                                              |
+---+-------------------+-------------------------------------------------------------------+

1.4.3. Starting Installation with Docker
'''''''''''''''''''''''''''''''''''''''''''''''''

In folder (where docker-compose.yml is located) run the command: ::
    
    docker -compose up –d

After changing «.env» file or other parameters, you need to start service with the rebuild key: ::

    docker -compose up -d –build

2. Service Scaling
-------------------------------

To process requests a mechanism for setting a problem and obtaining a result in a synchronous mode is implemented. To ensure the scale of the CARS.API system and its fault tolerance, an external HTTP request balancer (for example, NGINX) should be used.

Scaling within a single server (vertical scaling) is achieved by running multiple instances of CARS API and using a request balancer (Figure 1).

To run multiple instances of CARS API, open the /ansible/group_vars/all.yml file and set the number of instances using the «CARS_API_PORTS_RANGE» variable. Then you need to restart Ansible: ::

    ansible-playbook -hosts install_api.yml

3. Licensing
---------------------------

LUNA CARS uses the HASP license utility.

The system can operate in 2 modes – local and server. In the local mode, HASP is installed on each machine separately, in the server mode – only on the server.

3.1. HASP Installation and Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Before obtaining a license, you must install the HASP utility on the server. Move to the directory with the utility distribution /stream/extras/hasp and run the HASP installation. ::

    yum install -y haspd-7.60-vlabs.x86_64.rpm

After installation, you need to start HASP with the following commands: ::

    systemctl daemon-reload 
    systemctl start aksusbd 
    systemctl enable aksusbd

You can check the status of the utility using the command: ::

    systemctl status aksusbd

If the utility is successfully launched, the result of the executed command will be the following message: :: 

    aksusbd.service - LSB: Sentinel LDK RTE
        Loaded: loaded (/etc/rc.d/init.d/aksusbd; bad; vendor preset: disabled)
        Active: active (running) since Tue 2021-06-29 16:32:43 MSK; 1 day 19h ago
         Docs: man:systemd-sysv-generator(8)
        CGroup: /system.slice/aksusbd.service
           ├─909 /usr/sbin/aksusbd
           ├─920 /usr/sbin/winehasp
           └─953 /usr/sbin/hasplmd -s

3.2. License Activation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A system fingerprint file is required to obtain a license. It needs to be generated in the / hasp directory using the following script: ::

    ./LicenseAssist fingerprint <название_файла>.c2v

In some cases, an access error may occur. It is necessary to grant permissions to the script and repeat the process: ::

    chmod +x LicenseAssist

After the process of generating a system fingerprint is completed, it will need to added fingerprint to request for technical support.

Technical support staff will send an email with a license file in «.v2c» format. This file will need to be uploaded to the server with the HASP utility installed.

The license file must be added to HASP via the web interface. By default, when the HASP utility is launched, the interface is available at the following address: http://<IP_адрес>:1947/_int_/checkin.html.

On the «Update/Attach» page, you need to add the license file and apply the changes (Figure 3).

After applying file, the system should display a message about the successful completion of the operation.