Release Notes
=====================

LUNA CARS API 0.0.12
---------------------------

* Added support for the «grz_ai_recognition» classifier which implements the functionality of the «grz_all_countries» classifier without displaying the country.

* Fixed configuration files «modelRunner.cfg». Now for all countries except the UAE supports both Latin and Cyrillic characters.  

* Added «vehicle_ermergency_type» classifier. 

* Added licensing support.

LUNA CARS API 0.0.11
-------------------------

* Added support for recognition of the UAE license plate that can be enabled via configuration file «modelRunner.cfg»; 

* Added «uae_recognition_v1» and «grz_emirate_recognition_v1» classifiers.

LUNA CARS API 0.0.10
---------------------------

* Added the «grz_country_recognition_v1» classifier that recognize country of the license plate.

* Added new classifiers for recognition of the license plate: 

    * «eu_recognition_v1» – European Union;

    * «rus_spec_recognition_v1» – Emergency services vehicles;

    * «grz_bel_ukr_kzh_recognition_v1» – Belarus, Ukraine, Kazakhstan.

* Added «grz_all_countries» classifier that is used to determine whether the license plate belongs to a country, and then runs the recognition classifier for the corresponding country.

* Added support for avx2 for all recognition models, improving the speed of recognition by 2.5 times.

LUNA CARS API 0.0.9
---------------------------

* Added «vehicle_color» classifier.

* Added «vehicle_type» classifier.

* Added «vehicle_descriptor» classifier.

* The «car_brand_model» classifier has been updated. Support for 800 vehicle models has been implemented.

* Implemented support for selecting a network for each of the classifiers in configuration file «modelRunner.cfg».

* Added networks with avx2 support for some classifiers.

* Implemented the return of an error message if no network is found.

* Deleted «grz_ai_recognition» classifier.

* The result of «grz_ai_recognition_v2» classifier can be displayed in Cyrillic characters. 

LUNA CARS API 0.0.8
----------------------------

* Added record of event manager logs for recognition results. 

* Added support for /bulk_classify request.

* Added «car_brand_model» classifier. Support for 400 vehicle models has been implemented. Added «сar_brand_model» threshold to «modelRunner.cfg». If the recognition result is below the threshold, then «background» is returned.

* Added «useLatinCharacters» flag in «modelRunner.cfg». Results for «grz_ai_recognition» and «marka_taxi_mt» classifiers will be displayed in Latin or Cyrillic characters.

* Added «grz_ai_recognition_v2» classifier. The recognition result is displayed in Latin characters. 

LUNA CARS API 0.0.7
-------------------------------

* The GIL locking mechanism has been improved. 

* Increased the maximum processing time for images in python. 

* Fixed bugs with stop of the service. 

LUNA CARS API 0.0.6
------------------------------

* Added «solid_line_intersection» classifier, which determines if the car has crossed a solid line.

LUNA CARS API 0.0.5
-----------------------------

* The mechanism of cropping frames has been improved.

* Implemented conversion from BGR to RGB for the «speed_bad_good_spec» classifier. 

LUNA CARS API 0.0.4
-----------------------------

* Improved routing in the API.

LUNA CARS API 0.0.3
-----------------------------

* •	Added documentation.

LUNA CARS API 0.0.2
------------------------------

* Added «length_scores» field in «grz_ai_recognition» classifier. 

* Improved post-processing for «speed_bad_good_spec» classifier.

LUNA CARS API 0.0.1
-----------------------------

* Added «marka_taxi_mt» classifier that returns the brand of the vehicle and its belonging to the public transport.

* Added «pmt_grz_quality» classifier that evaluates the image quality of the GRZ. 

* Added «grz_ai_recognition» classifier that is used to recognize the license plate. 

* Added «speed_bad_good_spec» classifier that determines the speed violation code.

* Added «pmt_bad_photo» classifier that evaluates the image quality of the vehicle.

* Added «ts_bcd_type» classifier that determines the vehicle category.

